if(!window._genesys)window._genesys = {};
if(!window._gt)window._gt = [];

var agentConnected = false;
var USER_INFO_URL = null; // http://example.com
var userInfo = {
    userAgent: navigator.userAgent,
    screenResolution: window.innerWidth + 'x' + window.innerHeight,
};

window._genesys = {
    widgets: {
        main: {
            debug: false,
            theme: "light",
            lang: "en",
            i18n: "Resources/allegro/widgets/src/i18n/en.json",
            customStylesheetID: "genesys-widgets-allegro",
            plugins: [
                "cx-webchat",
                "cx-webchat-service"
            ]
        },
        webchat: {
            dataURL: "http://poz-gen-101.allegrotest.internal:9080/genesys/2/chat/customer-support",
            apikey: "",
            uploadsEnabled: true,
            emojis: false,
            actionsMenu: false,
            userData: {
                userId: '44056904',
				sendEmail: 'true'
            },
            autoInvite: {
                enabled: true,
                timeToInviteSeconds: 10,
                inviteTimeoutSeconds: 30
            },
            chatButton: {
                enabled: true,
                openDelay: 1000,
                effectDuration: 300,
                hideDuringInvite: true,
                template: '<span class="cx-icon" data-icon="chat"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="226.8px" height="226.8px" viewBox="0 0 226.8 226.8" style="enable-background:new 0 0 226.8 226.8;" xml:space="preserve" ><g> <g> <path class="st0" d="M24.8,158.7l0.6-35.5C14.6,119.4,6.8,109.1,6.8,97V27.7C6.8,12.4,19.2,0,34.4,0h101.7 c15.3,0,27.7,12.4,27.7,27.7V97c0,15.3-12.4,27.7-27.7,27.7h-69L24.8,158.7z M34.4,17.9c-5.4,0-9.7,4.4-9.7,9.7V97 c0,5.4,4.4,9.7,9.7,9.7h9.1l-0.2,14l17.5-14h75.3c5.4,0,9.7-4.4,9.7-9.7V27.7c0-5.4-4.4-9.7-9.7-9.7H34.4z"/> </g> <path class="st0" d="M193.7,79.9h-10.8v17.9h10.8c4.4,0,7.9,3.6,7.9,8v63c0,4.4-3.6,8-7.9,8h-9.1l0.2,11.8l-14.6-11.8h-68.4 c-4.4,0-7.9-3.6-7.9-8v-24.7H75.8v24.7c0,14.3,11.6,26,25.9,26h62.1l39.5,31.9l-0.5-33.5c9.8-3.7,16.8-13.2,16.8-24.3v-63 C219.6,91.6,208,79.9,193.7,79.9z"/> <g> <rect x="40.2" y="54" class="st0" width="17.7" height="17.7"/> </g> <g> <rect x="75.7" y="54" class="st0" width="17.7" height="17.7"/> </g> <g> <rect x="111.8" y="54" class="st0" width="17.7" height="17.7"/> </g> </g> </svg><span class="i18n cx-chat-button-label" data-message="ChatButton" tabindex="0">Rozpocznij czat</span></span>'
            },
            form: {
                wrapper: "<div class='wh-form-fields'></div>",
                inputs: [

                    {
                        id: "cx_webchat_form_firstname",
                        name: "firstname",
                        maxlength: "100",
                        placeholder: "@i18n:webchat.ChatFormPlaceholderFirstName",
                        label: "@i18n:webchat.ChatFormFirstName",
                        wrapper: "<div class='wh-form-fields__item'>{input}{label}</div>",
                        validate: function(event){
                            return window._genesys.helperMethods.floatingLabel(event);
                        }

                    },

                    {
                        id: "cx_webchat_form_lastname",
                        name: "lastname",
                        maxlength: "100",
                        placeholder: "@i18n:webchat.ChatFormPlaceholderLastName",
                        label: "@i18n:webchat.ChatFormLastName",
                        wrapper: "<div class='wh-form-fields__item'>{input}{label}</div>",
                        validate: function(event){
                            return window._genesys.helperMethods.floatingLabel(event);
                        }
                    },

                    {
                        id: "cx_webchat_form_email",
                        name: "email",
                        maxlength: "100",
                        placeholder: "@i18n:webchat.ChatFormPlaceholderEmail",
                        label: "@i18n:webchat.ChatFormEmail",
                        wrapper: "<div class='wh-form-fields__item'>{input}{label}</div>",
                        validate: function(event){
                            return window._genesys.helperMethods.floatingLabel(event);
                        }
                    },

                    {
                        id: "cx_webchat_form_subject",
                        name: "subject",
                        maxlength: "100",
                        placeholder: "@i18n:webchat.ChatFormPlaceholderSubject",
                        label: "@i18n:webchat.ChatFormSubject",
                        wrapper: "<div class='wh-form-fields__item'>{input}{label}</div>",
                        validate: function(event){
                            return window._genesys.helperMethods.floatingLabel(event);
                        }
                    }
                ]
            }
        }
    },
    helperMethods: {
        floatingLabel: function(e){

            function _goUp(){
                e.currentTarget.parentNode.classList.add('wh-focus');
            }
            function _goDown(){
                e.currentTarget.parentNode.classList.remove('wh-focus');
            }

            if(!!e && Object.keys(e).length !== 0){
                e.currentTarget.value !== '' ? _goUp() : _goDown();
            }
            return true;
        },
        showOptions: function(){
            document.querySelectorAll('.wh-message-menu__list')[0].style.display = 'block';
            document.querySelectorAll('.cx-upload')[0].style.right = '0';

            if(agentConnected) {
                window._genesys.helperMethods.removeDisabled();
            }
        },
        hideOptions: function(){
            if(document.querySelectorAll('.wh-message-menu__list')[0] !== undefined){
                document.querySelectorAll('.wh-message-menu__list')[0].style.display = 'none';
                document.querySelectorAll('.cx-upload')[0].style.right = '-9999999px';
            }
        },
        endChat: function() {
            CXBus.command('WebChat.endChat').done(function(e){
                window._genesys.helperMethods.hideOptions();
            });
        },
        removeDisabled: function() {
            if(document.querySelectorAll('.cx-upload')[0] !== undefined) {
                if(document.querySelectorAll('.cx-upload')[0].style.display === 'block' &&
                    document.querySelectorAll('.wh-message-menu__item .disabled-btn')[0] !== undefined){
                    document.querySelectorAll('.wh-message-menu__item .disabled-btn')[0].classList.remove('disabled-btn');
                }
            }
        },
        stylingLastSystemMessage: function() {
            setTimeout(function(){
                var messages = document.querySelectorAll('.cx-message.cx-system.i18n');
                if(messages.length > 0){
                    messages[messages.length - 1].classList.add('wh-message-system');
                }
            }, 50);
        },
        getBrowser: function () {
            var ua= navigator.userAgent, tem,
                M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
            if(/trident/i.test(M[1])){
                tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
                return 'IE '+(tem[1] || '');
            }
            if(M[1]=== 'Chrome'){
                tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
                if(tem!= null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
            }
            M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
            if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);

            userInfo.browser =  M.join(' ');
        },
        getIpLocation: function () {
            var xhr = new XMLHttpRequest();
            xhr.open('GET', 'http://ip-api.com/json/');
            xhr.onload = function() {
                if (xhr.status === 200) {
                    var result = JSON.parse(xhr.response);
                    userInfo.visitorNetwork = result.isp;
                    userInfo.ipAddress = result.query;
                    userInfo.location = {
                        city: result.city,
                        region: result.region,
                        country: result.country
                    };
                }
                else {
                    alert('Request failed.  Returned status of ' + xhr.status);
                }
            };
            xhr.send();
        },
        getUserInfo: function () {
            window._genesys.helperMethods.getIpLocation();
            window._genesys.helperMethods.getBrowser();
        },
        postUserInfo: function (){
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.open("POST", USER_INFO_URL);
            xmlhttp.setRequestHeader("Content-Type", "application/json");
            xmlhttp.send(JSON.stringify(userInfo));
        }
    }
};

if(!window._genesys.widgets.extensions){
    window._genesys.widgets.extensions = {};
}


window._genesys.helperMethods.getUserInfo();
window._genesys.widgets.extensions["whMenu"] = function($, CXBus, Common){

    var whMenu = CXBus.registerPlugin("whMenu");

    var iconChat = '<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="226.8px" height="226.8px" viewBox="0 0 226.8 226.8" style="enable-background:new 0 0 226.8 226.8;" xml:space="preserve" ><g> <g> <path class="st0" d="M24.8,158.7l0.6-35.5C14.6,119.4,6.8,109.1,6.8,97V27.7C6.8,12.4,19.2,0,34.4,0h101.7 c15.3,0,27.7,12.4,27.7,27.7V97c0,15.3-12.4,27.7-27.7,27.7h-69L24.8,158.7z M34.4,17.9c-5.4,0-9.7,4.4-9.7,9.7V97 c0,5.4,4.4,9.7,9.7,9.7h9.1l-0.2,14l17.5-14h75.3c5.4,0,9.7-4.4,9.7-9.7V27.7c0-5.4-4.4-9.7-9.7-9.7H34.4z"/> </g> <path class="st0" d="M193.7,79.9h-10.8v17.9h10.8c4.4,0,7.9,3.6,7.9,8v63c0,4.4-3.6,8-7.9,8h-9.1l0.2,11.8l-14.6-11.8h-68.4 c-4.4,0-7.9-3.6-7.9-8v-24.7H75.8v24.7c0,14.3,11.6,26,25.9,26h62.1l39.5,31.9l-0.5-33.5c9.8-3.7,16.8-13.2,16.8-24.3v-63 C219.6,91.6,208,79.9,193.7,79.9z"/> <g> <rect x="40.2" y="54" class="st0" width="17.7" height="17.7"/> </g> <g> <rect x="75.7" y="54" class="st0" width="17.7" height="17.7"/> </g> <g> <rect x="111.8" y="54" class="st0" width="17.7" height="17.7"/> </g> </g> </svg>';
    var iconArrowDown = '<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="226.8px" height="226.8px" viewBox="0 0 226.8 226.8" style="enable-background:new 0 0 226.8 226.8;" xml:space="preserve" ><g> <path class="st0" d="M112.7,179.8L3.6,68.4c-4.9-5-4.8-13,0.2-17.9c5-4.9,13-4.8,17.9,0.2l91.3,93.2l92.2-91.3 c5-4.9,13-4.9,17.9,0.1c4.9,5,4.9,13-0.1,17.9L112.7,179.8z"/> </g> </svg>';
    var iconMore = '<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="226.8px" height="226.8px" viewBox="0 0 226.8 226.8" style="enable-background:new 0 0 226.8 226.8;" xml:space="preserve" ><g> <circle class="st0" cx="113.4" cy="22.7" r="22.7"/> <circle class="st0" cx="113.4" cy="113.4" r="22.7"/> <circle class="st0" cx="113.4" cy="204.1" r="22.7"/> </g> </svg>';
    var iconClose = '<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="226.8px" height="226.8px" viewBox="0 0 226.8 226.8" style="enable-background:new 0 0 226.8 226.8;" xml:space="preserve" ><path class="st0" d="M133.6,113.4l88.4-89c5.6-5.6,5.6-14.7-0.1-20.3c-5.6-5.6-14.7-5.5-20.3,0.1l-88.3,88.8L25.1,4.2 C19.5-1.4,10.4-1.4,4.8,4.2s-5.6,14.7-0.1,20.3l88.4,89l-88.4,89c-5.6,5.6-5.6,14.7,0.1,20.3c2.8,2.8,6.4,4.2,10.1,4.2 c3.7,0,7.4-1.4,10.2-4.2l88.3-88.8l88.3,88.8c2.8,2.8,6.5,4.2,10.2,4.2c3.7,0,7.3-1.4,10.1-4.2c5.6-5.6,5.6-14.7,0.1-20.3 L133.6,113.4z"/> </svg>';


    whMenu.subscribe("WebChat.opened", function(e){

        // send user info
        if(!!USER_INFO_URL) {
            window._genesys.helperMethods.postUserInfo();
        }

        var inputContainer = document.querySelectorAll('.cx-input-container')[0];
        var div = document.createElement('div');
        div.setAttribute('class', 'wh-message-menu');
        var html = `<button class="wh-message-menu__btn" id="whoptionsbtn" onclick="window._genesys.helperMethods.showOptions();"><span class="wh-icon-settings"></span></button>` +
            '<ul class="wh-message-menu__list">' +
            '<li class="wh-message-menu__item"><a href="#" class="disabled-btn">Wyślij Plik</a></li>' +
            `<li class="wh-message-menu__item"><a href="#" onclick="window._genesys.helperMethods.endChat();">Zakończ Czat</li>` +
            '</ul>';


        div.insertAdjacentHTML( 'beforeend', html );

        inputContainer.appendChild(div);
        document.onclick = function(e){
            if(e.target.id !== 'whoptionsbtn'){
                window._genesys.helperMethods.hideOptions();
            }
        };

        document.querySelectorAll('.cx-titlebar .cx-icon')[0].innerHTML = iconChat;
        document.querySelectorAll('.cx-buttons-window-control .cx-button-minimize')[0].innerHTML = iconArrowDown;
        document.querySelectorAll('.cx-buttons-window-control .cx-button-close')[0].innerHTML = iconClose;
        window._genesys.helperMethods.stylingLastSystemMessage();
    });

    whMenu.subscribe("WebChatService.agentConnected", function(){
        agentConnected = true;
        window._genesys.helperMethods.stylingLastSystemMessage();
        window._genesys.helperMethods.removeDisabled();
    });

    //whMenu.subscribe("WebChatService.agentTypingStarted", function(){});
    //whMenu.subscribe("WebChatService.messageReceived", function(){});
    //whMenu.subscribe("WebChatService.pollingStopped", function(){});

    whMenu.subscribe("WebChatService.ended", function(){
        document.querySelectorAll('.wh-message-menu')[0].style.display = 'none';
        document.querySelectorAll('.cx-input-container')[0].style.display = 'none';
        window._genesys.helperMethods.hideOptions();

    });

    whMenu.subscribe("WebChat.minimized", function(){
        document.querySelectorAll('.cx-widget.cx-common-container')[0].classList.add('minimized-chat');
    });

    whMenu.subscribe("WebChat.unminimized", function(){
        document.querySelectorAll('.cx-widget.cx-common-container')[0].classList.remove('minimized-chat');
    });



    whMenu.republish("ready");
    whMenu.ready();
};