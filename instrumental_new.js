if(!window._genesys)window._genesys = {};
if(!window._gt)window._gt = [];

widgetCX = (function(){

    if(!window._genesys){
        window._genesys = {widgets: {}};
    }

    window._genesys.widgets = {
        main: {
            plugins: [
                "cx-sidebar",
                "cx-channel-selector",
                //"cx-webchat",
                //"cx-webchat-service",
                // "cx-send-message",
                //"cx-send-message-service",
                //"cx-gwe",
                //"cx-cobrowse",
                //"cx-calendar",
                //"cx-search",
                //"cx-knowledge-center-service",
                //"cx-chat-deflection",
                "cx-callback-service",
                "cx-stats-service",
                "cx-callback",
                //"cx-appointment",
                // "cx-offers",
                //"cx-preferences",
                //"cx-call-us"
            ],
            mobileMode: "auto",
            mobileModeBreakpoint: 600,
            theme: "light",
            themes: {
                genesys: 'cx-theme-genesys',
                green: 'cx-theme-green',
                brown: 'cx-theme-brown',
                gold: 'cx-theme-gold',
                red: 'cx-theme-red',
                blue: 'cx-theme-blue',
                cyan: 'cx-theme-cyan',
                purple: 'cx-theme-purple',
                black: 'cx-theme-black'
                //,custom: 'cx-theme-custom'
            },
            lang: "pl-PL",
            i18n: cxw_i18n,
            debug: true
        },
        sidebar: {
            showOnStartup: true,
            position: 'right',
            expandOnHover: true,
            channels: [{

                name: 'ChannelSelector',
                clickCommand: 'ChannelSelector.open',
                readyEvent: 'ChannelSelector.ready',
                clickOptions: {},

                displayName: '@i18n:channelselector.Title',
                displayTitle: '@i18n:channelselector.TitleDescription',
                icon: 'agent'
            }]
        },

        callback: {
            dataURL:'http://poz-gen-101.allegrotest.internal:9080/genesys/1/service/callback/callback-delay-agent' ,
            userData: {
                firstName: "",
                lastName: "",
                customerNumber: '',
                subject: "Allegro Callback",
                _target: "",
                mediaType: "callback"
            },
            countryCodes: false
        },
        calendar: {
            showAvailability: true,
            numberOfDays: 7,
            calenderHours: {
                interval: 10,
                morning: {
                    openTime: '09:00',
                    closeTime: '11:59'
                },
                afternoon: {
                    openTime: '12:00',
                    closeTime: '16:59'
                },
                evening: {
                    openTime: '17:00',
                    closeTime: '23:59'
                }
            }
        },

    };

    // Live person
    window._genesys.widgets.channelselector = {
        ewtRefreshInterval: 10,
        channels: []
    };

    window._genesys.widgets.channelselector.channels.push({
        enable: true,
        clickCommand: 'Callback.open',
        readyEvent: 'Callback.ready',
        displayName: 'Receive a Call',
        i18n: 'CallbackTitle',
        icon: 'call-incoming',
        html: '',
    });

    if(!window._genesys.widgets.extensions){
        window._genesys.widgets.extensions = {};
    }
})();