console.log('[CXW] init');
//var gdemoProxyData=readCookie("_GDemoProxy", true, false) || new Object();
//var CXW_THEME = getGdemoProxyData('cxw_theme', 'genesys');
var proactiveChat = false;
var CXW_ENABLED = true;
var widgetCssUrl='http://poz-gen-102.allegrotest.internal:8181/Resources/widgets.min.css';
var widgetJsUrl='http://poz-gen-102.allegrotest.internal:8181/Resources/widgets.min.js';
var proxyPrefsBaseUrl='http://poz-gen-101.allegrotest.internal:9080/';
var cx_converter = (typeof showdown != 'undefined') && new showdown.Converter();
//var oChatEnabled = false;


function createCookie(domain, name, value, isJson, escaped, expiration, expirationUnit) {
    console.log("[DemoAllegro] createCookie on domain="+domain+" name="+name+" value="+JSON.stringify(value));
    var expires;
    if (expiration) {
        var date = new Date();
        if (expirationUnit=='days')
            date.setTime(date.getTime()+(expiration*24*60*60*1000));
        else if (expirationUnit=='hours')
            date.setTime(date.getTime()+(expiration*60*60*1000));
        else if (expirationUnit=='minutes')
            date.setTime(date.getTime()+(expiration*60*1000));
        expires = "; expires="+date.toGMTString();
    }
    else {
        expires = "; expires=0;";
    }
    if (isJson)
        if (escaped)
            document.cookie = encodeURIComponent(name)+"="+encodeURIComponent(JSON.stringify(value))+expires+"; path=/;domain="+domain;
        else
            document.cookie = name+"="+JSON.stringify(value)+expires+"; path=/;domain="+domain;
    else
    if (escaped)
        document.cookie = encodeURIComponent(name)+"="+encodeURIComponent(value)+expires+"; path=/;domain="+domain;
    else
        document.cookie = name+"="+value+expires+"; path=/;domain="+domain;
};

function readCookie(name, isJson, escaped) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1,c.length);
        }
        if (c.indexOf(nameEQ) === 0) {
            if (isJson)
                if (escaped)
                    return JSON.parse(decodeURIComponent(c.substring(nameEQ.length,c.length)));
                else
                    return JSON.parse(c.substring(nameEQ.length,c.length));
            else
            if (escaped)
                return decodeURIComponent(c.substring(nameEQ.length,c.length));
            else
                return c.substring(nameEQ.length,c.length);
        }
    }
    return null;
}
/*M
function getGdemoProxyData(key, defaultVal) {
   var result=defaultVal;
   if (gdemoProxyData && gdemoProxyData.widget_settings && gdemoProxyData.widget_settings[key] != undefined){
       result=gdemoProxyData.widget_settings[key];
   }
   return result;
   }
   M*/
var domReady = function(callback) {
    var ready = false;

    var detach = function() {
        if(document.addEventListener) {
            document.removeEventListener("DOMContentLoaded", completed);
            window.removeEventListener("load", completed);
        } else {
            document.detachEvent("onreadystatechange", completed);
            window.detachEvent("onload", completed);
        }
    }
    var completed = function() {
        if(!ready && (document.addEventListener || event.type === "load" || document.readyState === "complete")) {
            ready = true;
            detach();
            callback();
        }
    };

    if(document.readyState === "complete") {
        callback();
    } else if(document.addEventListener) {
        document.addEventListener("DOMContentLoaded", completed);
        window.addEventListener("load", completed);
    } else {
        document.attachEvent("onreadystatechange", completed);
        window.attachEvent("onload", completed);

        var top = false;

        try {
            top = window.frameElement == null && document.documentElement;
        } catch(e) {}

        if(top && top.doScroll) {
            (function scrollCheck() {
                if(ready) return;

                try {
                    top.doScroll("left");
                } catch(e) {
                    return setTimeout(scrollCheck, 50);
                }

                ready = true;
                detach();
                callback();
            })();
        }
    }
}
function loadcss(url) {
    var head = document.getElementsByTagName('head')[0];
    link = document.createElement('link');
    link.type = 'text/css';
    link.rel = 'stylesheet';
    link.href = url;
    head.appendChild(link);
    return link;
}
if(!window._genesys)window._genesys = {};
if(!window._gt)window._gt = [];
domReady(function() {


    (function(a,t,c,l,o,u,d){a['altocloud-sdk.js']=o;a[o]=a[o]||function(){
        (a[o].q=a[o].q||[]).push(arguments)},a[o].l=1*new Date();u=t.createElement(c),
        d=t.getElementsByTagName(c)[0];u.async=1;u.src=l;d.parentNode.insertBefore(u,d)
        //})(window, document, 'script', 'https://altocloudcdn.com/sdk/js/web/v1/ac.js', 'ac');
    })(window, document, 'script', 'http://poz-gen-102.allegrotest.internal:8181/Resources/ac.js', 'ac');
    //ac('init', 'cje4xnbej1zt60hpwlo7hguob', {datacenter: 'us1'});
    ac('init', 'cje4xnbej1zt60hpwlo7hguob', {datacenter: 'us1', allowedLinkers: [/w*/], autoLink: [/w*/]});
    ac('pageview');
    ac('forms:track', 'form');


    $('body').append('<div id="GDemo-iFrame-div"><iframe id="GDemo-iFrame" src="'+proxyPrefsBaseUrl+'/gdc/SyncIframe.html" style="width:0;height:0;border:0; border:none;"></iframe></div>');

    function addQuadClickHandler() {
        $("html").on("quadclick", function() {
            if (true || confirm("Click OK to open the GDemo Proxy Configuration")) {
                popProxyPresWindow();
            }
        });
    }

    var addFunctionOnWindowLoad = function(callback){
        if(window.addEventListener){
            window.addEventListener('load',callback,false);
        }else{
            window.attachEvent('onload',callback);
        }
    }

    addFunctionOnWindowLoad(addQuadClickHandler);

    //           readGDemoCookies();

    if (document.createStyleSheet){ // for IE.
        document.createStyleSheet(widgetCssUrl);
    }
    else { // others
        loadcss(widgetCssUrl);
    }
})

widgetCX = (function(){
    /*
        var DEFAULT_SERVICE = "customer-support";
        var CHAT_URL = "http://poz-gen-101.allegrotest.internal:9080/genesys/2/chat/";
        var oPublished = {};
        var oWebChatControl = {};
        var oChatConfig = {

            //transport:  "gms",
            dataURL:    CHAT_URL + DEFAULT_SERVICE,
            userData: {
            chatType: "SALE",
            userID:"44056904"},
            emojis: true,
            actionsMenu: true,
            proactive: {
                enabled: false, // enable or disable
                idleTimer: 10,   // number of seconds of idle time before showing invite
                cancelTimer: 30 // number of seconds before invite auto-closes
            },
            chatButton: {

                    enabled: false,
                    template: false,
                    effect: 'fade',
    //				template: '<div>CHAT NOW</div>',
                    openDelay: 100,
                    effectDuration: 200,
                    hideDuringInvite: true
            },
            uploadsEnabled: true
        };
*/
    if(!window._genesys){
        window._genesys = {widgets: {}};
    }

    console.log('[CX] widget "pl-PL":'+"pl-PL");
    window._genesys.widgets = {
        main: {
            plugins: [
                "cx-sidebar",
                "cx-channel-selector",
                //"cx-webchat",
                //"cx-webchat-service",
                // "cx-send-message",
                //"cx-send-message-service",
                //"cx-gwe",
                //"cx-cobrowse",
                //"cx-calendar",
                //"cx-search",
                //"cx-knowledge-center-service",
                //"cx-chat-deflection",
                "cx-callback-service",
                "cx-stats-service",
                "cx-callback",
                //"cx-appointment",
                // "cx-offers",
                //"cx-preferences",
                //"cx-call-us"
            ],
            mobileMode: "auto",
            mobileModeBreakpoint: 600,
            theme: "light",
            themes: {
                genesys: 'cx-theme-genesys',
                green: 'cx-theme-green',
                brown: 'cx-theme-brown',
                gold: 'cx-theme-gold',
                red: 'cx-theme-red',
                blue: 'cx-theme-blue',
                cyan: 'cx-theme-cyan',
                purple: 'cx-theme-purple',
                black: 'cx-theme-black'
                //,custom: 'cx-theme-custom'
            },
            lang: "pl-PL",
            i18n: cxw_i18n,
            debug: true
        },
        sidebar: {
            showOnStartup: true,
            position: 'right',
            expandOnHover: true,
            channels: [{

                name: 'ChannelSelector',
                clickCommand: 'ChannelSelector.open',
                readyEvent: 'ChannelSelector.ready',
                clickOptions: {},

                //use your own static string or i18n query string for the below two display properties
                displayName: '@i18n:channelselector.Title',
                displayTitle: '@i18n:channelselector.TitleDescription',
                icon: 'agent'
            }
            ]
        },

        callback: {
            dataURL:'http://poz-gen-101.allegrotest.internal:9080/genesys/1/service/callback/callback-delay-agent' ,
            userData: {
                firstName: "",
                lastName: "",
                customerNumber: '',
                subject: "Allegro Callback",
                _target: "",
                mediaType: "callback"
            },
            countryCodes: false
            //apikey : "m6gLKXREBMOK8VAlygOHHLLn3eNkgYjG"
        },
        calendar: {
            showAvailability: true,
            numberOfDays: 7,
            calenderHours: {
                interval: 10, //min
                morning: {
                    openTime: '09:00',
                    closeTime: '11:59'
                },
                afternoon: {
                    openTime: '12:00',
                    closeTime: '16:59'
                },
                evening: {
                    openTime: '17:00',
                    closeTime: '23:59'
                }
            }
        },


        // webchat: oChatConfig,

    };


    // Stats
    window._genesys.widgets.stats = {
        ajaxTimeout: 3000,
        ewt: {
            dataURL: "http://poz-gen-101.allegrotest.internal:9080/genesys/1/service/ewt-for-vq"
            //apikey: 'n3eNkgLLgLKXREBMYjGm6lygOHHOK8VA'
        }
    };

    // Live person
    window._genesys.widgets.channelselector = {

        ewtRefreshInterval: 10,

        channels: [
        ]
    };

    window._genesys.widgets.channelselector.channels.push({
        enable: true,
        clickCommand: 'Callback.open',
        readyEvent: 'Callback.ready',
        displayName: 'Receive a Call',
        i18n: 'CallbackTitle',
        icon: 'call-incoming',
        html: '',
        /*                  ewt: {
                              display: false,
                              queue: 'GMS_VQ',
                              availabilityThresholdMin: 0,
                              availabilityThresholdMax: 1000000,
                              hideChannelWhenThresholdMax: false
                          }
         */             });


    /*                window._genesys.widgets.channelselector.channels.push({
                        enable: true,
                        clickCommand: 'WebChat.open',
                        readyEvent: 'WebChat.ready',
                        displayName: 'Web Chat',
                        i18n: 'ChatTitle',
                        icon: 'chat',
    */
    /*                   ewt: {
                           display: true,
                           queue: 'VQ_CHAT_SALE',
                           availabilityThresholdMin: 0,
                           availabilityThresholdMax: 50,
                           hideChannelWhenThresholdMax: true
                       } */
//                });





    if (proactiveChat) window._genesys.widgets.main.plugins.push("cx-gwe");

    window._genesys.widgets.onReady = function(oCXBus){
        console.log('[CXW] Widget bus has been initialized!');
        var oWCC = oCXBus;
        /*
                        // adding GWE IDs
                        _gt.push(['getIDs', function(IDs) {
                            console.log('[CXW] GWE IDs: ', JSON.stringify(IDs)); // getting GWE IDs
                            $.extend(oChatConfig.userData, IDs);
                            oWCC.command("WebChatService.configure", {"userData": oChatConfig.userData});
                        }]);
        */

        oWCC.command("WebChatService.registerPreProcessor", {preprocessor: function(oMessage){

                // Receive each chat message JSON as "oMessage"
                // Modify oMessage https://genesys.api.vidyocloud.com/flex.html?roomdirect.html&key=P1TvDysakk

                if(oMessage.from.type == 'Agent' && oMessage.type == "PushUrl" && oMessage.text) {
                    window.open(oMessage.text, '_blank');
                }
                else if(cx_converter && oMessage.from.type == 'Agent' && oMessage.text) {
                    oMessage.html = true;
                    oMessage.text = cx_converter.makeHtml(oMessage.text);
                }
                // Return it back to webchat
                return oMessage;
            }
        });

        // Sidebar localization (officially not supported)
        /*oWCC.subscribe("SideBar.opened", function(){
            try {
                $("div.cx-sidebar-button.cx-channel-selector > div.name").text(cxw_i18n[STR_LOCALE].channelselector.Title);
                $(" div.cx-sidebar-button.cx-search > div.name").text(cxw_i18n[STR_LOCALE].search.Title);
            }
            catch(e){}
        });
        */

        <!--Stef-->


        var chatFormEmailFieldSelector='#cx_webchat_form_email';
        var chatFormSubmitButtonSelector='.submit.cx-btn.cx-btn-primary.i18n';

    };

    if(!window._genesys.widgets.extensions){
        window._genesys.widgets.extensions = {};
    }

//            window._genesys.widgets.extensions["CXSpeechStorm"] = function($, CXBus, Common) {

    'use strict';


    var oWCC = null;
    var _activation = function(){

        console.log('[CXW] plugin initializing...');
        if (window._genesys.widgets.bus) {
            oWCC = window._genesys.widgets.bus; //.registerPlugin("WebChatControl");

            /*
                            oWCC.command("SideBar.addButton", {
                                button: {
                                        name: cxw_i18n[locale].webchat.ChatTitle,
                                        icon: "cx-img-map epicchat",
                                        class: "cx-test-button"
                                        }
                                })
                                .done(function(e){};
            */
            // prefilling
            //oWCC.command("Console.open");

            oWCC.subscribe("cx.plugin.Callback.opened", function(){
                _setFooter();
                //  _prefillForm();
                _sidebarHide();
            });

            oWCC.subscribe("cx.plugin.WebChat.opened", function(){
                _setFooter();
                // _prefillForm();
                _sidebarHide();
            });


            console.log('[CXW] The plugin is registered');
        }
        else {
            setTimeout(function(){widgetCX.activation();}, 200);
        }
    };

    var _sidebarShow = function () {
        //oWCC.command("cx.plugin.CXSideBar.open");
        //console.log("[CXW] The sidebar is shown");
    };

    var _sidebarHide = function () {
        //$(".cx-sidebar").hide(200);
        //oWCC.command("cx.plugin.CXSideBar.close");
        console.log("[CXW] The sidebar is hidden");
    };
    /*M
                var SaveGDemoConfig = function() {
                    var enabled = getGdemoProxyData('autofill', true);
                    if (!gdemoProxyData.widget_settings) {
                        gdemoProxyData.widget_settings={};
                        gdemoProxyData.widget_settings.cxw_chan_callback = gdemoProxyData.widget_settings.cxw_chan_callus = gdemoProxyData.widget_settings.cxw_chan_chat = gdemoProxyData.widget_settings.cxw_chan_cobrowse = gdemoProxyData.widget_settings.cxw_chan_email = gdemoProxyData.widget_settings.cxw_chan_video = gdemoProxyData.widget_settings.cxw_kc_search = gdemoProxyData.widget_settings.cxw_chat_kc_deflection =true;
                    }

                    gdemoProxyData.widget_settings.autofill = enabled;
                    gdemoProxyData.widget_settings.FirstName =  $("#cx_webchat_form_firstname").val() || $("#cx_firstname").val() || $("#cx_sendmessage_form_firstname").val() || $("#cx_form_callback_firstname").val() || gdemoProxyData.widget_settings.FirstName;
                    gdemoProxyData.widget_settings.LastName =  $("#cx_webchat_form_lastname").val() || $("#cx_lastname").val() || $("#cx_sendmessage_form_lastname").val() || $("#cx_form_callback_lastname").val() || gdemoProxyData.widget_settings.LastName;
                    gdemoProxyData.widget_settings.EmailAddress =  $("#cx_webchat_form_email").val() || $("#cx_sendmessage_from").val() || $("#cx_sendmessage_form_email").val() || $("#cx_form_callback_email").val() || gdemoProxyData.widget_settings.EmailAddress;
                    gdemoProxyData.widget_settings.PhoneNumber =  $("#cx_form_callback_phone_number").val() || gdemoProxyData.widget_settings.PhoneNumber;
                    console.log('[GDemo] Saving Form data into cookie _GDemoProxy='+JSON.stringify(gdemoProxyData));
                    createCookie(document.location.hostname, "_GDemoProxy", gdemoProxyData, true, false,1825, 'days');
                    var gdemoIframe = document.getElementById('GDemo-iFrame');
                    if (gdemoIframe) {
                        console.log('[GDemo] Sending message to embedded iFrame with cookie _GDemoProxy='+JSON.stringify(gdemoProxyData));
                        gdemoIframe.contentWindow.postMessage('setCookie:_GDemoProxy|true|false|'+JSON.stringify(gdemoProxyData), proxyPrefsBaseUrl);
                    }
                }
    M*/
    /*M           var _prefillForm = function () {
                   var enabled = getGdemoProxyData('autofill', true);
                   if (enabled) {
                       console.log('[GDemo] prefilling the form with gdemoProxyData='+JSON.stringify(gdemoProxyData));
                       $("#cx_webchat_form_firstname, #cx_firstname, #cx_sendmessage_form_firstname, #cx_form_callback_firstname").val(getGdemoProxyData('FirstName', ''));
                       $("#cx_webchat_form_lastname, #cx_lastname, #cx_sendmessage_form_lastname, #cx_form_callback_lastname").val(getGdemoProxyData('LastName', ''));
                       $("#cx_webchat_form_email, #cx_sendmessage_from, #cx_sendmessage_form_email, #cx_form_callback_email").val(getGdemoProxyData('EmailAddress', ''));
                       $("#cx_form_callback_phone_number").val(getGdemoProxyData('PhoneNumber', ''));
    M*/
    /*
    $("#cx_form_callback_subject").val("GBank Web Callback");
    $("#cx_webchat_form_subject").val("GBank Web Chat");
    $("#cx_sendmessage_subject, #cx_sendmessage_form_subject").val("GBank Web Message");
    */
    //M    }
    /*M               var submitButton=$(".cx-submit, .cx-callback-confirm");
                   if (submitButton)
                       submitButton.on("click",function() {
                           //alert ('submitted');
                           SaveGDemoConfig();
                       }); M*/
    //      }

    var _setFooter = function () {
        // $(".cx-powered-by").html("<img src='"+_demoHost+"/iisEpic/cxw/logo.png'></img>");
    };

    var _openChat = function(){

        if (! window._genesys.widgets.bus) {
            return; // not inited yet
        }

        var uData = {"_template": "GBank"};
        if (typeof additionalData != 'undefined') {
            uData = additionalData;
        }

        uData = $.extend(uData, oChatConfig.userData);
        oWCC.command("cx.plugin.WebChat.open", {"userData": uData});

    };


    // public interface
    oPublished = {
        activation: _activation,
        openChat: _openChat,
        setCustomData: function (udata) {
            oChatConfig.userData = $.extend(true, {}, udata);
        }
    };

    return oPublished;
})();
//        var smScript = document.createElement('script');
//        smScript.src = GaapJsUrl;

console.log('[CXW] init done');

// <!-- CX-Widget Lazy Load Instrumentation Script -->
if (CXW_ENABLED) {
    (function(d, s, id, o){var f = function(){var fs = d.getElementsByTagName(s)[0], e;
        if (d.getElementById(id)) return;e = d.createElement(s);
        e.id = id;e.src = o.src;fs.parentNode.insertBefore(e, fs);},ol = window.onload;

        if(o.onload){typeof window.onload != "function"?window.onload=f:window.onload=function(){ol();f();widgetCX.activation();}}else {f();widgetCX.activation();};})

    //		typeof window.onload !="function"?window.onload=f:window.onload=function(){ol();f();widgetCX.activation();}})
    (document,'script','genesys-cx-widget', {src: widgetJsUrl});
}

(window);


(function($)
{
    // Default options
    var defaults = {
        threshold: 1500 // ms
    }

    function quadcHandler(event)
    {
        var $elem = jQuery(this);

        // Merge the defaults and any user defined settings.
        settings = jQuery.extend({}, defaults, event.data);

        // Get current values, or 0 if they don't yet exist.
        var clicks = $elem.data("triclick_clicks") || 0;
        var start  = $elem.data("triclick_start")  || 0;

        // If first click, register start time.
        if (clicks === 0) { start = event.timeStamp; }

        // If we have a start time, check it's within limit
        if (start != 0
            && event.timeStamp > start + settings.threshold)
        {
            // Tri-click failed, took too long.
            clicks = 0;
            start  = event.timeStamp;
        }

        // Increment counter, and do finish action.
        clicks += 1;
        if (clicks === 4)
        {
            clicks     = 0;
            start      = 0;
            event.type = "quadclick";

            // Let jQuery handle the triggering of "quadclick" event handlers
            if (jQuery.event.handle === undefined) {
                jQuery.event.dispatch.apply(this, arguments);
            }
            else {
                // for jQuery before 1.9
                jQuery.event.handle.apply(this, arguments);
            }
        }

        // Update object data
        $elem.data("triclick_clicks", clicks);
        $elem.data("triclick_start",  start);
    }

    var quadclick = $.event.special.quadclick =
        {
            setup: function(data, namespaces)
            {
                $(this).bind("touchstart click.quadc", data, quadcHandler);
            },
            teardown: function(namespaces)
            {
                $(this).unbind("touchstart click.quadc", data, quadcHandler);
            }
        };
})(jQuery);